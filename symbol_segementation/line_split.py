import numpy as np
import cv2 as cv
from sklearn.cluster import KMeans, DBSCAN
import statistics
import itertools
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--filename", default='5.jpg')
args = parser.parse_args()
# img = cv.imread('img/out_test/Binary_image.jpg')
filename = args.filename
img = cv.imread('input/'+ filename)
height, width = np.size(img, 0), np.size(img, 1)
print(width, height)
# remove textbox
# offset = 20
# img = img[offset: width-offset,offset:height-offset]
def cv_contours(img):
    imgCopy = img.copy()
    imgray = cv.cvtColor(imgCopy, cv.COLOR_BGR2GRAY)
    ret, thresh = cv.threshold(imgray, 127, 255, 0)
    contours, hierarchy = cv.findContours(thresh, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
    return contours, hierarchy

def find_contours(img):
    # imgray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # ret, thresh = cv.threshold(imgray, 127, 255, 0)
    contours, hierarchy = cv_contours(img)
    # contours, hierarchy = cv.findContours(thresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
    # contours, hierarchy = cv.findContours(img, mode = CV_RETR_LIST, method = CV_CHAIN_APPROX_NONE)
    boundingBoxes = []
    boundingCnt = []
    for cnt in contours:
        approx = cv.approxPolyDP(cnt,1,True)
        box = cv.boundingRect(approx)
        
        x,y,w,h = box
        if w > width/2 or w <12 or h <2:

        # if w > width/2 or h > height/2 or w <2 or h <2:
            continue
        boundingBoxes.append(box)
        boundingCnt.append([[x,y],[x+w,y],[x+w,y+h], [x,y+h]])
    boundingCnt = np.array(boundingCnt)
    # boundingBoxes, _ =cv.groupRectangles(boundingBoxes,0,0.2)
    return boundingBoxes, boundingCnt


# expand for overlapping
boundingBoxes, boundingCnt = find_contours(img)

img_cnt = img.copy()
mask = np.zeros((height,width,3), np.uint8)
for i in range(len(boundingBoxes)):
    x,y,w,h = boundingBoxes[i]
    img_cnt = cv.drawContours(img_cnt, np.array([[[x,y],[x+w,y],[x+w,y+h], [x,y+h]]]), -1, (255,0,0), 1)
    cv.rectangle(mask, (x, y), (x+w, y+h), (255, 255, 255), -1)
cv.imshow("Display window", img_cnt)
k = cv.waitKey(0)

imgray_cnt = cv.cvtColor(mask, cv.COLOR_BGR2GRAY)
ret, thresh = cv.threshold(imgray_cnt, 127, 255, 0)
contours, hierarchy = cv.findContours(thresh, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

imgcp = img.copy()
cv.drawContours(imgcp, contours, -1, (0,255,0), 3)
# cv.imwrite(filename.split('.')[0]+ '_segmented' + ".jpg", imgcp)
cv.imshow("Display window", imgcp)
k = cv.waitKey(0)