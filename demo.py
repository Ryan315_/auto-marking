# sys package
import sys, os, time
import base64
import argparse
import requests
import json
# qt package
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtGui import QImage, QPixmap, QIcon, QPainter, QPen
from PyQt5.QtCore import Qt, pyqtSignal, QThread, pyqtSlot, QRect, QBasicTimer
from PyQt5.QtWidgets import QApplication, QFileDialog, QWidget, QLabel, QTextEdit, QAction, QTreeWidget, QTreeWidgetItem, QProgressBar
# tools
import matplotlib.pyplot as plt
import numpy as np
import cv2
from PIL import Image, ImageGrab
# model package
import torch
import torch.nn as nn
import torch.nn.functional as  F
import torch.optim as optim
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, Dataset
# OCR package
from mathpix.mathpix import MathPix
from cnocr import CnOcr
from cnstd import CnStd
from model_wcn.train import OCR
import tensorflow as tf
from sympy.parsing.latex import parse_latex
tf.get_logger().setLevel('ERROR')

# Common module for calling Mathpix OCR service from Python.
#
# N.B.: Set your credentials in environment variables APP_ID and APP_KEY,
# either once via setenv or on the command line as in
#

# ocr = CnOcr()
# res = ocr.ocr_for_single_line('input/input_3.jpg')
# print("Predicted Chars:", res.encode('utf-8'))



# env = os.environ

# default_headers = {
#     'app_id': env.get('APP_ID', APP_ID),
#     'app_key': env.get('APP_KEY', APP_KEY),
#     'Content-type': 'application/json'
# }

# class NetBig(nn.Module):
#     def __init__(self):
#         super(NetBig, self).__init__()
#         self.conv1 = nn.Conv2d(in_channels=1, out_channels=64, kernel_size=3, padding=1)
#         self.pool = nn.MaxPool2d(2, 2)
#         self.conv2 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding=1)
#         self.conv3 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, padding=1)
#         self.conv4 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, padding=1)
#         self.fc1 = nn.Linear(8192, 4096)
#         self.fc2 = nn.Linear(4096, 1024)
#         self.fc3 = nn.Linear(1024, args.num_class)
#         # self.dropout = nn.Dropout(p=0.8)

#     def forward(self, x):
#         x = self.pool(F.relu(self.conv1(x)))
#         x = self.pool(F.relu(self.conv2(x)))
#         x = self.pool(F.relu(self.conv3(x)))
#         x = self.pool(F.relu(self.conv4(x)))
#         x = x.view(-1, self.num_flat_features(x))
#         x = F.relu(self.fc1(x))
#         x = F.relu(self.fc2(x))
#         x = self.fc3(x)
#         return x

#     @staticmethod
#     def num_flat_features(x):
#         size = x.size()[1:]
#         num_features = 1
#         for s in size:
#             num_features *= s
#         return num_features


# class NetSmall(nn.Module):
#     def __init__(self):
#         super(NetSmall, self).__init__()
#         self.conv1 = nn.Conv2d(1, 6, 3)
#         self.pool = nn.MaxPool2d(2, 2)
#         self.conv2 = nn.Conv2d(6, 16, 5)
#         self.fc1 = nn.Linear(2704, 512)
#         self.fc2 = nn.Linear(512, 84)
#         self.fc3 = nn.Linear(84, args.num_class)

#     def forward(self, x):
#         x = self.pool(F.relu(self.conv1(x)))
#         x = self.pool(F.relu(self.conv2(x)))
#         x = x.view(-1, self.num_flat_features(x))
#         x = F.relu(self.fc1(x))
#         x = F.relu(self.fc2(x))
#         x = self.fc3(x)
#         return x

#     @staticmethod
#     def num_flat_features(x):
#         size = x.size()[1:]
#         num_features = 1
#         for s in size:
#             num_features *= s
#         return num_features

# def inference():
#     print('Start inference...')
#     transform = transforms.Compose([transforms.Resize((args.image_size, args.image_size)),
#                                     transforms.Grayscale(),
#                                     transforms.ToTensor()])

#     f = open(args.root + '/test.txt')
#     num_line = sum(line.count('\n') for line in f)
#     f.seek(0, 0)
#     line = int(torch.rand(1).data * num_line - 10) # -10 for '\n's are more than lines
#     while line > 0:
#         f.readline()
#         line -= 1
#     img_path = f.readline().rstrip('\n')
#     f.close()
#     label = int(img_path.split('/')[-2])
#     print('label:\t%4d' % label)
#     input = Image.open(img_path).convert('RGB')
#     input = transform(input)
#     input = input.unsqueeze(0)
#     model = NetSmall()
#     model.eval()
#     checkpoint = torch.load(args.log_path)
#     model.load_state_dict(checkpoint['model_state_dict'])
#     output = model(input)
#     _, pred = torch.max(output.data, 1)
    
#     print('predict:\t%4d' % pred)


service = 'https://api.mathpix.com/v3/latex'

image_scale_width = 1280/426.
image_scale_height = 720/240.

image_scale_width_r = 1280/640.
image_scale_height_r = 720/360.

class WebCam(QtWidgets.QWidget):
    def __init__(self,parent=None):
        super().__init__(parent)
        self.image = QtGui.QImage()

    def inference(self, image: np.ndarray):
        start_time = time.time()
        largest_face_portion = 0
        composite = image.copy()
        return composite

    def image_data_slot(self, image_data):
        image = self.inference(image_data)
        
        self.image = self.get_qimage(image)
        self.image = self.image.scaled(1080, 720, Qt.KeepAspectRatio)
        if self.image.size() != self.size():
            self.setFixedSize(self.image.size())
        self.update()

    def get_qimage(self, image: np.ndarray):
        height, width, colors = image.shape
        bytesPerLine = 3 * width
        QImage = QtGui.QImage

        image = QImage(image.data, width, height, bytesPerLine, QImage.Format_RGB888)
        image = image.rgbSwapped()
        return image
    
    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawImage(0, 0, self.image)
        self.image = QtGui.QImage()

class RecordVideo(QtCore.QObject):
    image_data = QtCore.pyqtSignal(np.ndarray)

    def __init__(self, camera_port=0, parent=None):
        super().__init__(parent)
        self.camera = cv2.VideoCapture(camera_port)
        self.camera.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*"MJPG"))
        self.camera.set(cv2.CAP_PROP_FRAME_WIDTH,1280)
        self.camera.set(cv2.CAP_PROP_FRAME_HEIGHT,720)
        self.timer = QtCore.QBasicTimer()

    def start_recording(self):
        self.timer.start(0, self)

    def timerEvent(self, event):
        if (event.timerId() != self.timer.timerId()):
                return

        read, image = self.camera.read()
        image = cv2.flip(image, 1)
        if read:
            self.image_data.emit(image)

class ImageBoard(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
 
    def initUI(self):

        hbox = QHBoxLayout(self)
        pixmap = QPixmap('input.jpg')
    
        lb1 = QLabel(self)
        lb1.setPixmap(pixmap)
    
        hbox.addWidget(lb1)
        self.setLayout(hbox)

class OCR_mathpix():
    def __init__(self, parent=None):
        self.default_headers = {
            'app_id': 'pec12138_gmail_com_554f66_d5b7b9',
            'app_key': '19da6c93dc563d06b2e3',
            'Content-type': 'application/json'
            }

        self.mathpix = MathPix(self.default_headers['app_id'], self.default_headers['app_key'])

    def ocr_lib(self, filename):
        self.ocr = self.mathpix.process_image(image_path=filename)

    def get_latex(self):
        return ocr.latex

    def get_confidence(self):
        return ocr.latex_confidence

    # processing images
    def image_uri(filename):
        image_data = open(filename, "rb").read()
        return "data:image/jpg;base64," + base64.b64encode(image_data).decode()
    #
    # Call the Mathpix service with the given arguments, headers, and timeout.
    #
    def latex(self, args, timeout=30):
        headers=self.default_headers
        r = requests.post(service,
            data=json.dumps(args), headers=headers, timeout=timeout)
        return json.loads(r.text)


    def mathpix_clipboard(): # Identify the clipboard formula
        im = ImageGrab.grabclipboard()
        fp = os.path.abspath(__file__)
        fp = os.path.dirname(fp) # Get the file dir. 
        fn = os.path.join(fp, 'equa.png') # Get the filename where the picture is stored.
        im.save(fn,'PNG')
        r = latex({
            'src': image_uri(fn),
            'formats': ['latex_simplified']
        })
        print(r['latex_simplified'])

class MyLabel(QLabel):
    def __init__(self, pbar=None):
        super(MyLabel, self).__init__()
        self.x0=0
        self.y0=0
        self.x1=0
        self.y1=0
        self.pos0=None
        self.pos1=None
        self.flag=False
        self.isShow=True
        self.image = QtGui.QImage()
        self.image_latex = QtGui.QImage()
        self.shape = [0, 0, 0]
        self.rectangles = []
        self.pbar = pbar

    def inference(image_data, width = None, height = None, inter = cv2.INTER_AREA):
        # initialize the dimensions of the image to be resized and
        # grab the image size
        dim = None
        print('---------',image_data.shape)
        (h, w) = image.shape[:2]

        # if both the width and height are None, then return the
        # original image
        if width is None and height is None:
            return image

        # check to see if the width is None
        if width is None:
            # calculate the ratio of the height and construct the
            # dimensions
            r = height / float(h)
            dim = (int(w * r), height)

        # otherwise, the height is None
        else:
            # calculate the ratio of the width and construct the
            # dimensions
            r = width / float(w)
            print(h*r)
            dim = (width, int(h * r))

        # resize the image
        resized = cv2.resize(image, dim, interpolation = inter)

        # return the resized image
        return resized

    def image_preprocessing(self, _write=True):
        image_data = self._image
        self.shape = image_data.shape
        self.image_right = np.zeros(self.shape, np.uint8)
        self.image_right.fill(255)
        self.pbar.setText('processing: image crop')

        # mypath = 'stuAns'
        # stuId = 'stu1'
        # questionId = '17a'

        # _mathpix = OCR_mathpix()
        # ocr = _mathpix.mathpix.process_image(image_data=self.byte_image)

        # ocr_latex = ocr.latex
        # print(ocr_latex)
        # ocr_split = ocr_latex.split('=')
        # print(ocr_split)
        # convert_sym = []
        # # write_line = {}
        # for ii in ocr_split:
        #     expr = parse_latex(ii)
        #     convert_sym.append(str(expr))
        # print(convert_sym)
        # write_line = '='.join(convert_sym)

        # with open(os.path.join(mypath, stuId, questionId+'.txt'), 'w') as file:
        #     file.write(write_line)

        # self.random_crop(image_data, 50)   
        box_info_list = self.ocr_line_segmentation(image_data)
        # # cn OCR model
        # args = dict()
        # gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
        # config = tf.ConfigProto(gpu_options=gpu_options)
        # config.gpu_options.allow_growth = True
        # config.gpu_options.visible_device_list = str(0)
        # args['model_save_path'] = 'model_wcn/model'
        # args['input_height'] = 64
        # args['input_weight'] = 64
        # args['class_num'] = 3755
        # args['init_lr'] = 1e-2

        # sess = tf.Session(config= config)
        # model = OCR(args,sess)

        # # test_input = cv2.imread('temp_output/16.jpg', 0)
        # # predict = model(test_input)
        # # print(predict)
        # #--------------------------------------------------------------------------------------

        i = 0
        
        for box_info in box_info_list:
            predict = []
            i = i+1
            # print(box_info['box'])
            # self.x0 = box_info['box'][0][0]
            # self.y0 = box_info['box'][0][1]
            # self.x1 = box_info['box'][2][0]
            # self.y1 = box_info['box'][2][1]
            # self.rectangles.append(QRect(self.x0,self.y0,(self.x1-self.x0),(self.y1-self.y0)))
        #     print(box_info['cropped_img'].shape)
        #     self.image_right[self.y0:self.y1, self.x0:self.x1] = box_info['cropped_img']
            # cv2.imshow('test',box_info['cropped_img'])
            # cv2.waitKey()

            # _position = self.hist_crop(box_info['cropped_img'])
        
            # for ii in _position:
            #     # cv2.imshow('test', box_info['cropped_img'][ii[1]:ii[3], ii[0]:ii[2]])
            #     ocr_input = cv2.cvtColor(box_info['cropped_img'][ii[1]:ii[3], ii[0]:ii[2]], cv2.COLOR_BGR2GRAY)
            #     # predict.append(model(ocr_input))
            # print('symbol position of line-', str(i),':')
            # # print(_position)
            # if i in range(4,13):
            #     predict = []
            # print(predict)
            cv2.imwrite('temp_output/'+str(i)+'.jpg' , box_info['cropped_img'])
            print(i)
                # for m in range(len(_position)):
                #     img = cv2.rectangle(box_info['cropped_img'], (_position[m][0],_position[m][1]), (_position[m][2],_position[m][3]), (255, 0, 0), 1)
                # cv2.imwrite('temp_output/'+str(i)+'_crop.jpg' , img)

        # os.system("fim temp_output/mathpix_out.png")    

        # sess.close()

        print('Done!')
        return self.image_right

    def cnocr_written(self, img_list):
        args = dict()
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
        config = tf.ConfigProto(gpu_options=gpu_options)
        config.gpu_options.allow_growth = True
        config.gpu_options.visible_device_list = str(0)
        args['model_save_path'] = 'model_wcn/model'
        args['input_height'] = 64
        args['input_weight'] = 64
        args['class_num'] = 3755
        args['init_lr'] = 1e-2
        with tf.Session(config = config) as sess:
            model = OCR(args,sess)
            # test(model)

            predict = model('input/jia.png')
            print('predict:', predict)

    def hist_crop(self, image_data):
        Position = []
        W = self.getVProjection(image_data)
        height, width = image_data.shape[:2]

        Wstart = 0
        Wend = 0
        W_Start = 0
        W_End = 0
        for j in range(len(W)):
            if W[j] > 0 and Wstart ==0:
                W_Start =j
                Wstart = 1
                Wend=0
            if W[j] <= 0 and Wstart == 1:
                W_End =j
                Wstart = 0
                Wend=1
            if Wend == 1:
                Position.append([W_Start,0,W_End, height])
                Wend =0
            # cv2.imshow('crop', img)
            # cv2.waitKey()

        return Position

    def getVProjection(self, image_data):
        image_gray = cv2.cvtColor(image_data, cv2.COLOR_BGR2GRAY)
        retval, image = cv2.threshold(image_gray,127,255,cv2.THRESH_BINARY_INV)
        # cv2.imshow('gray image', image)
        vProjection = np.zeros(image.shape,np.uint8);

        (h,w) = image.shape[:2]
        w_ = [0]*w
        for x in range(w):
            for y in range(h):
                if image[y,x] == 255:
                    w_[x]+=1

        for x in range(w):
            for y in range(h-w_[x],h):
                vProjection[y,x] = 255
        # cv2.imshow('vProjection',vProjection)
        # cv2.waitKey()
        return w_

    def random_crop(self, image_data, n_crop=50):
        height, width = image_data.shape[:2]
        print(height, width)
        for ii in range(n_crop):
            pass

    def ocr_line_segmentation(self, input):
        # set ENV LANG to be utf-8
        ## use sys.getfilesystemencoding() to check the system encoding 
        ## have corrected the encoding in dockerfile
        s = "中文输出测试"
        print(s)
        std = CnStd()
        box_info_list = std.detect(input, pse_threshold=0.25)
        return box_info_list

    def ocr_symbol_segmentation(self, input):
        pass

    def ocr_symbol_recognition(self, img):
        # for print cn symbol
        ocr = CnOcr()
        res = ocr.ocr_for_single_line('input/1785.png')
        return res

    def slot_image_left(self, image_path):
        self.rectangles.clear()
        self.ori_image = cv2.imread(image_path)
        with open(image_path, "rb") as f:
            self.byte_image = f.read()
        # resize and keep ratio
        _temp = self.ori_image.copy()
        _temp_ratio = _temp.shape[1]/_temp.shape[0]
        if _temp_ratio >= 720/480:
            _temp_resize = (720, int(720/_temp_ratio))
        else:
            _temp_resize = (int(480*_temp_ratio), 480)
        # _image = self.inference(_temp, height=800)
        self._image = cv2.resize(_temp, _temp_resize, interpolation = cv2.INTER_AREA)
        print(self._image.shape)
        
        self.image = self.get_qimage(self._image)
        self.update()

        # self.image_preprocessed = self.image_preprocessing(_image, True)
        # self.image_latex = self.get_qimage(self.image_preprocessed)

        self.x0=0
        self.y0=0
        self.x1=0
        self.y1=0
        # self.image = self.image.scaled(700, 720, Qt.KeepAspectRatio)
        print('-------- image loaded')

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawImage(0, 0, self.image)
        painter.setPen(QPen(Qt.red,2,Qt.SolidLine))
        # painter.drawImage(self.shape[1]+10, 0, self.image_latex)
        for _rect in self.rectangles:
            painter.drawRect(_rect)
        rect=QRect(self.x0,self.y0,(self.x1-self.x0),(self.y1-self.y0))
        painter.drawRect(rect)
        self.load_new = False


    def get_qimage(self, image: np.ndarray):
        height, width, colors = image.shape
        bytesPerLine = 3 * width
        QImage = QtGui.QImage

        image = QImage(image.data, width, height, bytesPerLine, QImage.Format_RGB888)
        image = image.rgbSwapped()
        return image
 
    def mousePressEvent(self, event):
        QLabel.mousePressEvent(self,event)
        self.flag=True
        self.pos0=event.globalPos()
        self.x0=event.x()
        self.y0=event.y()
 
    def mouseMoveEvent(self, event):
        QLabel.mouseMoveEvent(self,event)
        if self.flag:
            self.pos1 = event.globalPos()
            self.x1=event.x()
            self.y1=event.y()
            self.update()
 
    def mouseReleaseEvent(self, event):
        QLabel.mouseReleaseEvent(self,event)
        self.rectangles.append(QRect(self.x0,self.y0,(self.x1-self.x0),(self.y1-self.y0)))
        self.image_preprocessed[self.y0:self.y1, self.x0:self.x1] = self.ori_image[self.y0:self.y1, self.x0:self.x1]
        self.image_latex = self.get_qimage(self.image_preprocessed)
        self.update()
        # self.flag=False
        # self.isShow = False
 
    def getRectGlobalPos(self):
        poses=[(self.pos0.x(),self.pos0.y())]
        poses.append((self.pos1.x(),self.pos1.y()))
        return poses

    def drawrectangle(self, img, color, thickness, width_scale, height_scale, isClosed=False):
        cv2.rectangle(img, (x,y), (x+w,y+h), color, thickness)

class MyProgressBar(QProgressBar):
    def __init__(self):
        super().__init__()
        self._text = None
    
    def setText(self, text):
        self._text = text
    
    def text(self):
        return self._text

class MainWidget(QtWidgets.QWidget):
    _sig_tree = QtCore.pyqtSignal(str)
    _sig_image = QtCore.pyqtSignal(np.ndarray)
    _sig_progress = QtCore.pyqtSignal(str)
    _sig_image_path = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(MainWidget, self).__init__(parent)
        self.structure_view = QTreeWidget()

        self.load_path_str = './input'
        self.slot_load_path(self.load_path_str)

        self.pbar = MyProgressBar()
        self.image_board_left = MyLabel(self.pbar)
        self.image_board_left.setScaledContents(True)
        self.algo_widget = WebCam() #left plot board

        # Create and set the layout
        layout = QtWidgets.QGridLayout()
        layout.setSpacing(10)
        layout.addWidget(self.structure_view, 0, 1, 8, 2)
        layout.addWidget(self.image_board_left, 0, 3, 7, 10)
        layout.addWidget(self.pbar,7,3,1,10)
        # layout.addWidget(self.image_board_right, 0, 6, 0, 8) 

        # for testing framework
        self.record_video = RecordVideo(camera_port=0)
        self.run_button = QtWidgets.QPushButton('Start')

        # Connect the image data signal and slot together
        ## video processing
        image_data_slot = self.algo_widget.image_data_slot
        self.record_video.image_data.connect(image_data_slot)

        ## load input image
        self._sig_tree.connect(self.slot_load_path)
        self.structure_view.itemDoubleClicked.connect(self.handler)

        ## image processing and demonstration
        self._sig_image_path.connect(self.image_board_left.slot_image_left)

        self.setLayout(layout)

    # for sidebar QTreeWidget 
    def triger_file(self):
        _dir = QtWidgets.QFileDialog.getExistingDirectory(None, 'Select project folder:', './', QtWidgets.QFileDialog.ShowDirsOnly)
        self._sig_tree.emit(_dir)

    def slot_load_path(self, path):
        self.path = path
        self.structure_view.clear()
        self.load_project_structure(self.path, self.structure_view)
        # self.update

    def load_project_structure(self, startpath, tree):
        pass
        for element in os.listdir(startpath):
            path_info = startpath + "/" + element
            parent_itm = QTreeWidgetItem(tree, [os.path.basename(element)])
            parent_itm.setData(0, Qt.UserRole, path_info)
            if os.path.isdir(path_info):
                self.load_project_structure(path_info, parent_itm)
                parent_itm.setIcon(0, QIcon('./ico/folder.ico'))
            else:
                parent_itm.setIcon(0, QIcon('./ico/file.ico'))
        
    def handler(self, item, column_no):
        # _qpix = QPixmap(item.data(0, Qt.UserRole))
        # image = cv2.imread(item.data(0, Qt.UserRole))
        image_path = item.data(0, Qt.UserRole)
        # self.image_board.setPixmap(_qpix)
        self._sig_image_path.emit(image_path)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__()

        self.setGeometry(300, 300, 1200, 850)
        self.setWindowTitle('Auto-Marking demo')
        # define main widgets
        self.form_widget = MainWidget(self)
        self.setCentralWidget(self.form_widget)
        self.initUI()

    def initUI(self):
        self.statusBar()

        openFile = QAction(QIcon('ico/icon_open.png'), 'Open', self)
        # openFile.setShortcut('Ctrl+O')
        # openFile.setStatusTip('Open file')
        openFile.triggered.connect(self.form_widget.triger_file)

        toolbar_openfile = self.addToolBar('Open')
        toolbar_openfile.addAction(openFile)
        # self.form_widget.load_path('/home/ryan90')

        exitAction = QAction(QIcon('ico/icon_exit.png'), 'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setStatusTip('Exit application')
        exitAction.triggered.connect(self.close)

        toolbar_exit = self.addToolBar('Exit')
        toolbar_exit.addAction(exitAction)
        
        opencam = QAction(QIcon('ico/webcam.png'), 'webcam', self)
        opencam.setShortcut('Ctrl+Q')
        opencam.setStatusTip('Exit application')
        opencam.triggered.connect(self.form_widget.record_video.start_recording)
        
        toolbar_webcam = self.addToolBar('Webcam')
        toolbar_webcam.addAction(opencam)

        startprocess = QAction(QIcon('ico/start.ico'), 'start', self)
        startprocess.triggered.connect(self.form_widget.image_board_left.image_preprocessing)
        toolbar_start = self.addToolBar('Start')
        toolbar_start.addAction(startprocess)

    def slot_test(self):
        pass

def main():
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
