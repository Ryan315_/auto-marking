<!--
 * @Author: Ryan Yu
 * @Date: 2021-01-18 00:03:43
 * @LastEditTime: 2021-01-18 02:19:43
 * @LastEditors: Ryan Yu
 * @Description: Descriptions for auto-marking demo.
 * @FilePath: /auto-marking/README.md
 * @---------------------------------
-->
# README #

## 1. Use docker repo to build docker environment for demo.


## 2. Main function.
>python3 demo.py

## 3. Processing pipeline.
![pipeline](figure/pipeline.jpg){width=50%}