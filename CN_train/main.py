import os
import torch
import torch.nn as nn
import torch.nn.functional as  F
import torch.optim as optim
import torchvision.transforms as transforms
from torch.utils.data import DataLoader, Dataset
from PIL import Image
import argparse
from tqdm import tqdm

parse = argparse.ArgumentParser(description='Params for training. ')

parse.add_argument('--root', type=str, default='./data/HWDB1/', help='path to data set')
parse.add_argument('--mode', type=str, default='train', choices=['train', 'validation', 'inference'])
parse.add_argument('--log_path', type=str, default=os.path.abspath('.') + '/pth', help='dir of checkpoints')

parse.add_argument('--restore', type=bool, default=False, help='whether to restore checkpoints')

parse.add_argument('--batch_size', type=int, default=32, help='size of mini-batch')
parse.add_argument('--image_size', type=int, default=64, help='resize image')
parse.add_argument('--epoch', type=int, default=100)
parse.add_argument('--num_class', type=int, default=3755, choices=range(10, 3755))
parse.add_argument('--input', type=str, default='../input/jia.jpg')
parse.add_argument('--load_pth', type=str, default='./pth/checkpoint_1.pth')
args = parse.parse_args()

class MyDataset(Dataset):
    def __init__(self, txt_path, num_class, transforms=None):
        super(MyDataset, self).__init__()
        images = []
        labels = []
        with open(txt_path, 'r') as f:
            for line in f:
                if int(line.split('/')[-2]) >= num_class:  # just get images of the first #num_class
                    break
                line = line.strip('\n')
                images.append(line)
                labels.append(int(line.split('/')[-2]))
        self.images = images
        self.labels = labels
        self.transforms = transforms

    def __getitem__(self, index):
        image = Image.open(self.images[index]).convert('RGB')
        label = self.labels[index]
        if self.transforms is not None:
            image = self.transforms(image)
        return image, label

    def __len__(self):
        return len(self.labels)

class ResidualBlock(nn.Module):
    def __init__(self, inchannel, outchannel, stride=1):
        super(ResidualBlock, self).__init__()
        self.left = nn.Sequential(
            nn.Conv2d(inchannel, outchannel, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(outchannel),
            nn.ReLU(inplace=True),
            nn.Conv2d(outchannel, outchannel, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(outchannel)
        )
        self.shortcut = nn.Sequential()
        if stride != 1 or inchannel != outchannel:
            self.shortcut = nn.Sequential(
                nn.Conv2d(inchannel, outchannel, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(outchannel)
            )

    def forward(self, x):
        out = self.left(x)
        out += self.shortcut(x)
        out = F.relu(out)
        return out

class ResNet(nn.Module):
    def __init__(self, ResidualBlock, num_classes=121):
        super(ResNet, self).__init__()
        self.inchannel = 64
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU(),
        )
        self.layer1 = self.make_layer(ResidualBlock, 64,  2, stride=1)
        self.layer2 = self.make_layer(ResidualBlock, 128, 2, stride=2)
        self.layer3 = self.make_layer(ResidualBlock, 256, 2, stride=2)
        self.layer4 = self.make_layer(ResidualBlock, 512, 2, stride=2)
        self.fc = nn.Linear(512, num_classes)

    def make_layer(self, block, channels, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)   #strides=[1,1]
        layers = []
        for stride in strides:
            layers.append(block(self.inchannel, channels, stride))
            self.inchannel = channels
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv1(x)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out


def ResNet18():

    return ResNet(ResidualBlock)


class NetBig(nn.Module):
    def __init__(self):
        super(NetBig, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=64, kernel_size=3, padding=1)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=3, padding=1)
        self.conv3 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=3, padding=1)
        self.conv4 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=3, padding=1)
        self.fc1 = nn.Linear(8192, 4096)
        self.fc2 = nn.Linear(4096, 1024)
        self.fc3 = nn.Linear(1024, args.num_class)
        # self.dropout = nn.Dropout(p=0.8)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = self.pool(F.relu(self.conv3(x)))
        x = self.pool(F.relu(self.conv4(x)))
        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    @staticmethod
    def num_flat_features(x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


class NetSmall(nn.Module):
    def __init__(self):
        super(NetSmall, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 3)
        self.bn1 = nn.BatchNorm2d(6)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 3)
        self.bn2 = nn.BatchNorm2d(16)
        self.dropout = nn.Dropout(0.25)
        self.fc1 = nn.Linear(576, 576)
        # self.fc1 = nn.Linear(2704, 512)
        # self.fc2 = nn.Linear(512, 84)
        self.fc3 = nn.Linear(576, args.num_class)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.pool(F.relu(x))
        x = self.conv2(x)
        x = self.bn2(x)
        x = self.pool(F.relu(x))

        x = x.view(-1, self.num_flat_features(x))
        x = F.relu(self.fc1(x))
        x = self.dropout(x)
        # x = F.relu(self.fc2(x))
        # x = self.dropout(x)
        x = self.fc3(x)
        return x

    @staticmethod
    def num_flat_features(x):
        size = x.size()[1:]
        num_features = 1
        for s in size:
            num_features *= s
        return num_features


def train():

    transform_train = transforms.Compose([transforms.Resize((args.image_size, args.image_size)),
                                    transforms.Grayscale(),
                                    transforms.RandomHorizontalFlip(),
                                    transforms.RandomCrop(32),
                                    transforms.ToTensor()])
    # transform_train = transforms.Compose([
    #     transforms.Resize((args.image_size, args.image_size)),
    #     transforms.RandomCrop(32),  #先四周填充0，在吧图像随机裁剪成32*32
    #     transforms.RandomHorizontalFlip(),  #图像一半的概率翻转，一半的概率不翻转
    #     transforms.ToTensor(),
    #     transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)), #R,G,B每层的归一化用到的均值和方差
    #     ])

    train_set = MyDataset(args.root + '/train.txt', num_class=args.num_class, transforms=transform_train)
    train_loader = DataLoader(train_set, batch_size=args.batch_size, shuffle=True)

    print('number of classes:', args.num_class)
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    print(device)

    model = NetSmall()
    model.to(device)

    model.train()

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    # if args.load_pth:
    #     print('loading checkpoint:', args.load_pth)
    #     checkpoint = torch.load(args.load_pth)
    #     model.load_state_dict(checkpoint['model_state_dict'])
    #     optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    #     loss = checkpoint['loss']
    #     epoch = checkpoint['epoch']
    # else:
    loss = 0.0
    epoch = 0
    
    total = 0.0
    correct = 0.0
    while epoch < args.epoch:
        running_loss = 0.0
        count = 0
        with tqdm(train_loader, unit = "batch") as tepoch:
            # for i, data in enumerate(train_loader):
            for i, data in enumerate(tepoch):
                inputs, labels = data[0].to(device), data[1].to(device)

                optimizer.zero_grad()
                outs = model(inputs)

                _, predict = torch.max(outs.data, 1)
                total += labels.size(0)
                correct += sum(predict == labels).item()

                loss = criterion(outs, labels)
                loss.backward()
                optimizer.step()

                running_loss += loss.item()
                count = i

        print('epoch %5d: batch: %5d, loss: %f' % (epoch, count, running_loss / count))     
        print('Training: Accuracy: %.2f%%' % (correct / total * 100))

        # if epoch % 5 == 1:
        print('Save checkpoint...')
        save_pth = 'checkpoint_' + str(epoch) + '.pth'
        torch.save({'epoch': epoch,
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    'loss': loss},
                    os.path.join(args.log_path, save_pth))
            
        validation(epoch)

        epoch += 1

    print('Finish training')


def validation(epoch=0):
    transform_test = transforms.Compose([transforms.Resize((32, 32)),
                                    transforms.Grayscale(),
                                    transforms.ToTensor()])
    # transform_test = transforms.Compose([transforms.Resize((32, 32)),
    #         transforms.ToTensor(),
    #         transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    #     ])

    test_set = MyDataset(args.root + '/test.txt', num_class=args.num_class, transforms=transform_test)
    test_loader = DataLoader(test_set, batch_size=args.batch_size)

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model = NetSmall()
    # model = ResNet18()
    model.to(device)
    
    load_pth = 'checkpoint_' + str(epoch) + '.pth'
    checkpoint = torch.load(os.path.join(args.log_path, load_pth))
    model.load_state_dict(checkpoint['model_state_dict'])

    model.eval()

    total = 0.0
    correct = 0.0
    with torch.no_grad():
        with tqdm(test_loader, unit = "batch") as tepoch:
            for i, data in enumerate(tepoch):
                inputs, labels = data[0].cuda(), data[1].cuda()
                outputs = model(inputs)
                _, predict = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += sum(predict == labels).item()

                # if i % 100 == 99:
                #     print('batch: %5d,\t acc: %f' % (i + 1, correct / total))
    print('Validation: Accuracy: %.2f%%' % (correct / total * 100))


def inference():
    print('Start inference...')
    transform = transforms.Compose([transforms.Resize((args.image_size, args.image_size)),
                                    transforms.Grayscale(),
                                    transforms.ToTensor()])

    # f = open(args.root + '/test.txt')
    # num_line = sum(line.count('\n') for line in f)
    # f.seek(0, 0)
    # line = int(torch.rand(1).data * num_line - 10) # -10 for '\n's are more than lines
    # while line > 0:
    #     f.readline()
    #     line -= 1
    # img_path = f.readline().rstrip('\n')
    # f.close()
    # label = int(img_path.split('/')[-2])
    # print('label:\t%4d' % label)
    input = Image.open(args.input).convert('RGB')
    input = transform(input)
    input = input.unsqueeze(0)
    model = NetBig()
    model.eval()
    checkpoint = torch.load(args.load_pth)
    model.load_state_dict(checkpoint['model_state_dict'])
    output = model(input)
    _, pred = torch.max(output.data, 1)
    print(output)
    print('predict:\t%4d' % pred)



def classes_txt(root, out_path, num_class=None):
    '''
    write image paths (containing class name) into a txt file.
    :param root: data set path
    :param out_path: txt file path
    :param num_class: how many classes needed
    :return: None
    '''
    dirs = os.listdir(root)
    if not num_class:
        num_class = len(dirs)

    if not os.path.exists(out_path):
        f = open(out_path, 'w')
        f.close()

    with open(out_path, 'r+') as f:
        try:
            end = int(f.readlines()[-1].split('/')[-2]) + 1
        except:
            end = 0
        if end < num_class - 1:
            dirs.sort()
            dirs = dirs[end:num_class]
            for dir in dirs:
                files = os.listdir(os.path.join(root, dir))
                for file in files:
                    f.write(os.path.join(root, dir, file) + '\n')


if __name__ == '__main__':

    classes_txt(args.root + '/train', args.root + '/train.txt', num_class=args.num_class)
    classes_txt(args.root + '/test', args.root + '/test.txt', num_class=args.num_class)

    if args.mode == 'train':
        train()
    elif args.mode == 'validation':
        validation()
    elif args.mode == 'inference':
        inference()