from sympy import *
from os import listdir
import os.path
from collections import defaultdict
import numpy as np
import nltk
from nltk.corpus import words
from nltk.metrics.distance import edit_distance, jaccard_distance
from nltk.util import ngrams


class Marking:
    def __init__(self):
        self.markingFiles = defaultdict(dict)

    def check(self, ans, extracted, thres = 0):
        # minus them and check if it the difference less than the threshold
        # defualt threshold is 0
        try:
            # print('===',ans, extracted)
            if 0<= expand('('+ans+')-('+extracted+')') <= thres:
                return True
            else:
                return False
        except:
            return False
        # return abs(expand('('+ans+')-('+extracted+')')) <= thres

    def processAns(self, Ans, Stu):
        if Ans == Stu:
            return True
        inputAns = Ans.split('=')
        inputStudent = Stu.split('=')
        if len(inputAns) == 1:
            ans1 = inputAns[0]
            ans2 = inputAns[0]
        elif len(inputAns) > 1:
            # ans1 = '('+inputAns[0]+')-('+inputAns[1]+')'
            ans1 = inputAns[0]#+'-('+inputAns[1]+')'
            ans2 = inputAns[-1]
        if len(inputStudent) == 1:
            extracted1 = inputStudent[0]
            extracted2 = inputStudent[0]
        elif len(inputStudent) > 1:
            # extracted1 = '('+inputStudent[0]+')-('+inputStudent[1]+')'
            extracted1 = inputStudent[0]#+'-('+inputAns[1]+')'
            extracted2 = inputStudent[-1]
        # print(ans1, ans2, extracted1, extracted2)
        if len(inputAns) == 1 and len(inputStudent) == 1:
            res = self.check(ans1, extracted1)
        elif len(inputAns) == 1 and len(inputStudent) > 1:
            res = self.check(ans1, extracted2)
        elif len(inputAns) > 1 and len(inputStudent) == 1:
            res = self.check(ans2, extracted1)
        else:
            res = self.check(ans1+'+'+extracted2, extracted1+'+'+ans2) #or check(ans1, extracted2) or check(ans2, extracted1) or check(ans2, extracted2) 
        # if res:
        #     print(Stu + " for " + Ans + ": Correct")
        # else:
        #     print(Stu + " for " + Ans + ": Wrong")
        return res

    def readMarkingFiles(self, mypath = './markingScale'):
        onlyfiles = [f for f in listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
        # print(onlyfiles)
        for f in onlyfiles:
            key = f.split('.')[0]
            with open(os.path.join(mypath, f)) as file:
                curGrade = 0
                wholeFile = file.read()
                grades = wholeFile.split('##')[1:]
                for idx, g in enumerate(grades):
                    splits = g.split('\n')
                    splits = [s for s in splits if len(s)>0]
                    curGrade = splits[0]
                    curAnses = splits[1:]
                    self.markingFiles[key][idx] = (curGrade, curAnses)
        #         print(grades)
        # print(self.files)

    def saveMarkingFiles(self, filename = 'markingFiles'):
        np.save(filename+".npy", self.markingFiles)

    def loadMarkingFiles(self, filename = 'markingFiles'):
        markingFiles=np.load(filename+".npy", allow_pickle=True)
        self.markingFiles = markingFiles.item()
        return markingFiles
    

    def checkStu(self, stuId, questionId, mypath = './stuAns', gram_number = 3, thres = 0.3, thresUncertain = 0.2):
        # file = os.path.join(mypath, stuId, questionId+'.txt')
        # print(file)
        with open(os.path.join(mypath, stuId, questionId+'.txt')) as file:
            # curGrade = 0
            filelines = file.readlines()
            filelines = [f for f in filelines if '=' in f]
            filelines = [f.split('\n')[0] for f in filelines]
            # print(filelines)
            out = [0]*len(filelines)
            for idx, stuAns in enumerate(filelines):
                answers = [v[1] for v in self.markingFiles[questionId].values()]
                answers = [j for sub in answers for j in sub]
                # print(answers)
                # distances = ((jaccard_distance(stuAns, word), word) for word in answers)
                # distances = (jaccard_distance(set(stuAns), set(word)) for word in answers)
                # distances = [jaccard_distance(set(ngrams(stuAns, gram_number)),
                #                              set(ngrams(word, gram_number)))
                #                               for word in answers]
                # distances = [(edit_distance(stuAns, ans)/len(stuAns), ans) for ans in answers]
                # distances = [a[1] for a in distances if a[0]<=0.1]

                # find possible matches
                possileMatches = [ans for ans in answers if edit_distance(stuAns, ans)/len(stuAns) <= thres]
                # check if stuAns is close enough to answers
                possileUncertain = min([(edit_distance(stuAns, ans)/len(stuAns), ans) for ans in answers])
                # print(possileUncertain, stuAns, len(possileUncertain[1]), len(stuAns), possileUncertain[0] <= thresUncertain)
                flag = 0 # 0 for not related; 1 for correct; 2 for wrong; 3 for not sure
                if len(possileMatches) == 0:
                    flag = 0
                else:
                    check = False
                    for match in possileMatches:
                        # print(match, stuAns, self.processAns(stuAns, match))
                        check = check or self.processAns(stuAns, match)
                    if check:
                        flag = 1
                    elif not check and possileUncertain[0] <= thresUncertain and not len(possileUncertain[1]) == len(stuAns):
                        flag = 3
                    else:
                        flag = 2
                out[idx] = flag

            print("0 for not related; 1 for correct; 2 for wrong; 3 for not sure.")
            for i in range(len(filelines)):
                print(filelines[i], out[i])
        return out

if __name__ == "__main__":
    mark = Marking()
    mark.readMarkingFiles()
    mark.saveMarkingFiles()
    _ = mark.loadMarkingFiles()
    # print(mark.markingFiles)
    print('================================')
    out = mark.checkStu('stu1', '17a')
    print('================================')
    out = mark.checkStu('stu1', '18a')
    print('================================')
    out = mark.checkStu('stu2', '17a')
    print('================================')
    out = mark.checkStu('stu2', '18a')






    # test_checkAns()